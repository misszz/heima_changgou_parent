package com.changgou.utils;

import com.changgou.file.FastDFSFile;
import org.csource.common.NameValuePair;
import org.csource.fastdfs.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;

import java.io.*;

/**
 * @author zhongxuwang
 * @since 5:09 下午
 */
public class FastDFSUtil {

    //加载tracker
    static {
        //获取tracker的配置文件fdfs_client.conf的位置
        String filePath = new ClassPathResource("fdfs_client.conf").getPath();
        //加载tracker配置信息
        try {
            ClientGlobal.init(filePath);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * 上传文件
     *
     * @param fastDFSFile
     * @return 返回String[] uploads
     * uploads[0] 组名：类似group1
     * uploads[1] 文件名：
     * @throws Exception
     */
    public static String[] upload(FastDFSFile fastDFSFile) throws Exception {
        NameValuePair[] nameValuePairs = new NameValuePair[1];
        nameValuePairs[0] = new NameValuePair("author", fastDFSFile.getAuthor());
        //创建tracker客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过tracker客户端对象获取tracker服务端
        TrackerServer trackerServer = trackerClient.getConnection();
        //获取StorageClient对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        //执行文件上传
        String[] uploads = storageClient.upload_file(fastDFSFile.getContent(),
                fastDFSFile.getExt(),
                nameValuePairs);
        return uploads;
    }

    /***
     * 获取文件信息   http://192.168.45.242:8080/group1/M00/00/00/wKgt8mBV5QKAWjJjABGdb2MjzbM728.png"
     * @param groupName 组名  group1
     * @param remoteFileName 文件的信息  M00/00/00/wKgt8mBV5QKAWjJjABGdb2MjzbM728.png
     * @return
     */
    public static FileInfo getFile(String groupName, String remoteFileName) throws Exception {
        TrackerServer trackerServer = getTrackerServer();
        //有了tracker服务端对象，可以创建storage客户端对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        FileInfo fileInfo = storageClient.get_file_info(groupName, remoteFileName);
        return fileInfo;
    }

    /**
     * 获取trackerServer
     *
     * @return
     * @throws IOException
     */
    private static TrackerServer getTrackerServer() throws IOException {
        //创建tracker客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过tracker客户端对象，获取tracker服务端对象
        TrackerServer trackerServer = trackerClient.getConnection();
        return trackerServer;
    }

    /**
     * 获取Storage组信息
     *
     * @param groupName
     * @return
     */
    public static StorageServer getStorageServer(String groupName) throws Exception {
        //创建tracker客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过tracker客户端对象，获取tracker服务端对象
        TrackerServer trackerServer = trackerClient.getConnection();
        return trackerClient.getStoreStorage(trackerServer);
    }

    /***
     * 获取Tracker服务地址
     */
    public static String getTrackerUrl() throws Exception{
        //创建tracker客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过tracker客户端对象，获取tracker服务端对象
        TrackerServer trackerServer = trackerClient.getConnection();
        String ip = trackerServer.getInetSocketAddress().getAddress().getHostAddress();
        int port = ClientGlobal.getG_tracker_http_port();
        return "http://" + ip + ":" + port;
    }


    /**
     * 根据文件组名和文件存储路径获取Storage服务的IP、端口信息
     *
     * @param groupName      组名  group1
     * @param remoteFileName 文件的信息  M00/00/00/wKgt8mBV5QKAWjJjABGdb2MjzbM728.png
     * @return
     */
    public static ServerInfo[] getServerInfo(String groupName, String remoteFileName) throws Exception {
        //创建tracker客户端对象
        TrackerClient trackerClient = new TrackerClient();
        //通过tracker客户端对象，获取tracker服务端对象
        TrackerServer trackerServer = trackerClient.getConnection();
        ServerInfo[] serverInfos = trackerClient.getFetchStorages(trackerServer, groupName, remoteFileName);
        return serverInfos;
    }


    /**
     * 下载文件
     *
     * @param groupName      组名  group1
     * @param remoteFileName 文件的信息  M00/00/00/wKgt8mBV5QKAWjJjABGdb2MjzbM728.png
     * @return 返回文件字节的输入流
     */
    public static ByteArrayInputStream downloadFile(String groupName, String remoteFileName) throws Exception {
        //获取tracker服务端对象
        TrackerServer trackerServer = getTrackerServer();
        //获取storage客户端对象
        StorageClient storageClient = new StorageClient(trackerServer, null);
        byte[] fileBytes = storageClient.download_file(groupName, remoteFileName);
        return new ByteArrayInputStream(fileBytes);
    }

    /**
     * 删除文件
     *
     * @param groupName      组名  group1
     * @param remoteFileName 文件的信息  M00/00/00/wKgt8mBV5QKAWjJjABGdb2MjzbM728.png
     * @throws Exception
     */
    public static void deleteFile(String groupName, String remoteFileName) throws Exception {
        //获取tracker服务端对象
        TrackerServer trackerServer = getTrackerServer();
        StorageClient storageClient = new StorageClient(trackerServer, null);
        storageClient.delete_file(groupName, remoteFileName);
    }

    public static void main(String[] args) throws Exception {
        //获取文件的信息   http://192.168.45.242:8080/group1/M00/00/00/wKgt8mBWKwqAZUVsABGdb2MjzbM086.png
        String groupName = "group1";
        String remoteFileName = "M00/00/00/wKgt8mBWNQaAPcHoABGdb2MjzbM848.png";
        FileInfo fileInfo = getFile(groupName, remoteFileName);
        System.out.println(fileInfo.getSourceIpAddr());
        System.out.println(fileInfo.getFileSize());
        System.out.println("----------------获取storage组信息----------------------");
        //获取storage组信息
        StorageServer storageServer = getStorageServer(groupName);
        System.out.println(storageServer.getStorePathIndex());
        System.out.println("---------------根据文件组名和文件存储路径获取Storage服务的IP、端口信息-----------------------");
//        根据文件组名和文件存储路径获取Storage服务的IP、端口信息
        ServerInfo[] serverInfo = getServerInfo(groupName, remoteFileName);
        for (ServerInfo info : serverInfo) {
            System.out.println(info.getIpAddr());
            System.out.println(info.getPort());
        }
        System.out.println("----------------下载文件----------------------");
        ByteArrayInputStream bis = downloadFile(groupName, remoteFileName); //首先将storage文件输入到程序中
        byte[] buffer = new byte[1024 * 1024];
        File file = new File("/Users/zhongxuwang/Desktop/gitProject/111.jpg");
        FileOutputStream fos = new FileOutputStream(file);
        while (bis.read(buffer) != -1) {
            fos.write(buffer);
        }
        //刷新缓冲区
        fos.flush();
        //关闭输入输出流
        bis.close();
        fos.close();

        //删除文件
        deleteFile(groupName, remoteFileName);
    }
}
