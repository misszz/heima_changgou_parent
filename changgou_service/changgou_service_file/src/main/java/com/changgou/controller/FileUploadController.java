package com.changgou.controller;

import com.changgou.common.pojo.Result;
import com.changgou.common.pojo.StatusCode;
import com.changgou.file.FastDFSFile;
import com.changgou.utils.FastDFSUtil;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


/**
 * @author zhongxuwang
 * @since 5:34 下午
 */
@RestController
@RequestMapping("file")
@CrossOrigin
public class FileUploadController {
    @PostMapping("/upload")
    public Result upload(@RequestParam("file") MultipartFile file) throws Exception {
        //封装上传对象
        FastDFSFile fastDFSFile = new FastDFSFile(
                file.getOriginalFilename(),
                file.getBytes(),
                StringUtils.getFilenameExtension(file.getOriginalFilename())
        );
        String[] upload = FastDFSUtil.upload(fastDFSFile);
        String url = FastDFSUtil.getTrackerUrl()+ "/" + upload[0] + "/" + upload[1];
        return new Result(true, StatusCode.OK, "上传成功", url);
    }


}
